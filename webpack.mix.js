const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/assets/js')
    .sass('resources/assets/sass/app.scss', 'public/assets/css');

/*style login*/

mix.styles([
    'resources/assets/css/sb-admin-2.min.css',
    'resources/assets/vendor/fontawesome-free/css/all.min.css'
], 'public/assets/css/login.css').version();

mix.scripts([
    'resources/assets/vendor/jquery/jquery.min.js',
    'resources/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
    'resources/assets/vendor/jquery-easing/jquery.easing.min.js',
    'resources/assets/js/sb-admin-2.min.js',
], 'public/assets/js/login.js').version();

/*end style login*/

/*style error 403*/
mix.styles([
    'resources/assets/css/error403.css',
], 'public/assets/css/error403.css').version();
/*end error 403*/