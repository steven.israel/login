<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/login.css')}} " />

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Restablecer contraseña') }}</h1>
                  </div>
                  <form method="POST" action="{{ route('password.update') }}" autocomplete="off" class="user">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                          <input name="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror" value="{{ $email ?? old('email') }}" id="exampleInputEmail" aria-describedby="emailHelp" autocomplete="email" placeholder="Ingresa tu correo" required  autofocus>
                          @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <input type="password" id="password" name="password" class="form-control form-control-user @error('password') is-invalid @enderror" placeholder="Ingresa tu contraseña" required autocomplete="current-password">
                          @error('password')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <input type="password" id="password-confirm" name="password_confirmation" class="form-control form-control-user"placeholder="Ingresa tu contraseña" required autocomplete="new-password">
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-user btn-block">
                            {{ __('Restablecer') }}
                          </button>
                        </div>
                    </form>
                  <hr>
                  <div class="text-center">
                    @if (Route::has('password.request'))
                      <a class="btn btn-link" href="{{ route('login') }}">
                        {{ __('Login') }}
                      </a>
                    @endif
                  </div>
                  <!-- <div class="text-center">
                    <a class="small" href="register.html">Create an Account!</a>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <script src="{{ asset('assets/js/login.js') }}"></script>

</body>

</html>
