<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

<h1>Requerimientos del servidor</h1>
<p>El marco de trabajo Laravel tiene algunos requisitos del sistema. La máquina virtual Laravel Homestead cumple todos estos requisitos , 
por lo que se recomienda que utilice Homestead como su entorno de desarrollo local de Laravel.
Sin embargo, si no está utilizando Homestead, deberá asegurarse de que su servidor cumpla con los siguientes requisitos:</p>

<h4>
<p>PHP> = 7.1.3</p>
<p>Extensión PHP BCMath</p>
<p>Extensión PHP Ctype</p>
<p>Extensión PHP JSON</p>
<p>Extensión PHP Mbstring</p>
<p>Extensión PHP OpenSSL</p>
<p>PDO PHP Extension</p>
<p>Extensión PHP Tokenizer</p>
<p>Extensión XML PHP</p>
</h4>

<h2>Forma para instalar con <a href='https://laragon.org/download/'>Laragon Full</a> o la forma tradicional instalar los paquetes de apache con php y 
mysql segun la version de laravel.</h2>

<hr>
<h1>Contenido:</h1>
<p>1- Inicio de sesion</p>
<p>2- Registro de usuarios</p>
<p>3- Restableciemiento de contraseña</p>


