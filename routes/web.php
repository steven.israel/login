<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/403', 'HomeController@error_403')->name('403');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/administrador', 'HomeController@admin')->name('administrador');
});

Route::group(['middleware' => ['user']], function () {
    Route::get('/demo', 'HomeController@demo')->name('demo');
});